package Page;

import InitialDriver.Element;
import InitialDriver.InitialDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Page1 extends Element {

        public void GoToGoogle()
        {
            navigate("https://www.google.by");
            getWebElement(By.id("lst-ib")).sendKeys("Test Two");
        }

    public void GoToWiki()
    {
        navigate("https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0");
        getWebElement(By.id("searchInput")).sendKeys("Prototype Page Two");
    }
}
