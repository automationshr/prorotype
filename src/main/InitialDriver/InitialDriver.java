package InitialDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Sergey Potapov
 */
public class InitialDriver implements Copyable{


    public static final String CHROME_DRIVER_PATH = System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe";
    public static final String DRIVER_NAME_CHROME = "webdriver.chrome.driver";
    /**
     * There is setting driver by name
     */
    public synchronized WebDriver setDriver() {
        System.setProperty(DRIVER_NAME_CHROME, CHROME_DRIVER_PATH);
        WebDriver driver = new ChromeDriver();
        return driver;
    }
    public InitialDriver copy() {
        return new InitialDriver();
    }
}
