package InitialDriver;

public interface Copyable {
    Copyable copy();
}
