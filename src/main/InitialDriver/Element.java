package InitialDriver;

import InitialDriver.InitialDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Element  {

    private InitialDriver prototype= new InitialDriver();
    private WebDriver driver = prototype.copy().setDriver();
   protected void navigate(String url)
   {
       driver.navigate().to(url);
   }
   protected WebElement getWebElement(By locator)
   {
     return   driver.findElement(locator);
   }

}
