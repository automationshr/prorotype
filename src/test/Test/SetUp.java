package Test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

public class SetUp  {

    @AfterMethod
    public void closeDriver()
    {
        System.out.println("After Method");
    }

    @AfterTest
    public void closeDriverTest()
    {
        System.out.println("After Test");
    }
}
